package inc.sneid.kudaStorm.repo;

import inc.sneid.kudaStorm.domain.Post;
import inc.sneid.kudaStorm.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepo extends JpaRepository<Post, Long> {

    Page<Post> findByAuthor(User user, Pageable pageable);

}
