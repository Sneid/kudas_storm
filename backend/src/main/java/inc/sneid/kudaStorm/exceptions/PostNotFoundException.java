package inc.sneid.kudaStorm.exceptions;

public class PostNotFoundException extends RuntimeException{

    public PostNotFoundException(long id){
        super("Couldn't find post with id: " + id);
    }

}
