package inc.sneid.kudaStorm.exceptions;

public class CommonWebException extends RuntimeException {

    public CommonWebException(String message){
        super(message);
    }

}
