package inc.sneid.kudaStorm.service;

import inc.sneid.kudaStorm.domain.Post;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.dto.PostPageDto;
import inc.sneid.kudaStorm.exceptions.CommonWebException;
import inc.sneid.kudaStorm.exceptions.PostNotFoundException;
import inc.sneid.kudaStorm.repo.PostRepo;
import inc.sneid.kudaStorm.utils.PostValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {

    @Autowired
    private PostRepo postRepo;

    public Post getPost(long id){
        return postRepo.findById(id).orElseThrow(() -> new PostNotFoundException(id));
    }

    public PostPageDto getPostPage(int pageId, int size){
        Pageable pageRequest = PageRequest.of(pageId, size, Sort.Direction.DESC, "id");
        Page<Post> page = postRepo.findAll(pageRequest);
        return new PostPageDto(page);
    }

    public Post savePost(Post post, User author){
        if(!PostValidation.isValid(post)){
            throw new CommonWebException("Text in your post must be between 1 and 200 character(\\w in regexp)");
        }
        post.setAuthor(author);
        return postRepo.save(post);
    }

    public Post savePost(Post post){
        if(!PostValidation.isValid(post)){
            throw new CommonWebException("Text in your post must be between 1 and 200 character(\\w in regexp)");
        }
        return postRepo.save(post);
    }

    public Page<Post> findUserPost(User user, int pageId, int size){
        Pageable pageRequest = PageRequest.of(pageId, size, Sort.Direction.DESC, "id");
        return postRepo.findByAuthor(user, pageRequest);
    }

    public void deletePost(Post post){
        postRepo.delete(post);
    }

}
