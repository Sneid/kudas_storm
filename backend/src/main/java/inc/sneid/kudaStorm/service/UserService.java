package inc.sneid.kudaStorm.service;

import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.dto.UserDto;
import inc.sneid.kudaStorm.exceptions.UserNotFoundException;
import inc.sneid.kudaStorm.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static inc.sneid.kudaStorm.utils.CredentialsValidation.isValidUsername;

@Service
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    /**
     * Позволяет получить объект пользователя по его имени
     * @param s
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepo.findByUsername(s).orElseThrow(() -> new UserNotFoundException(s));
    }

    /**
     * Позволяет получить объект пользователя по его id
     * @param id
     * @return
     */
    public User findById(long id){
        return userRepo.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    /**
     * Позволяет сохранить пользователя в базе данных
     * @param user
     * @return
     */
    public User save(User user){
        return userRepo.save(user);
    }

    /**
     * Берет данные из newData и ставит их в user, если они валидны и сохраняет в базе данных
     * @param newData
     * @param user
     * @return
     */
    public User update(UserDto newData, User user){
        if (newData == null || newData.getId() == null) return user;
        if (isValidUsername(newData.getUsername())){
            user.setUsername(newData.getUsername());
        }
        return userRepo.save(user);
    }

    /**
     * Удаляет указанный объект пользователя
     * @param user
     */
    public void remove(User user){
        userRepo.delete(user);
    }

    /**
     * @param username Позволяет получить объект пользователя по его имени
     * @return
     */
    public User findByUsername(String username){
        return userRepo.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
    }

    /**
     * @param username
     * @return Отвечает на вопрос: "Существует ли пользователь с данным именем?"
     */
    public boolean isExistsUserWithUsername(String username){
        try {
            findByUsername(username);
            return true;
        }catch (UserNotFoundException e){
            return false;
        }
    }

    /**
     * @return Возвращает всех пользователей
     */
    public List<User> findAll() {
        return userRepo.findAll();
    }

    /**
     * @param string
     * @param pageId
     * @param size
     * @param ignoreCase
     * @return
     */
    public Page<User> findUserByUsernameStartsWith(String string, int pageId, int size, boolean ignoreCase){
        Pageable pageable = PageRequest.of(pageId, size, Sort.Direction.ASC, "username");
        return ignoreCase ?
                userRepo.findByUsernameStartingWithIgnoreCase(string, pageable)
                :
                userRepo.findByUsernameStartingWith(string, pageable);
    }

}
