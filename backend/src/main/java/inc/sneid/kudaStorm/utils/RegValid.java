package inc.sneid.kudaStorm.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegValid {
    public static boolean fullValid(String regexp, String string){
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
}
