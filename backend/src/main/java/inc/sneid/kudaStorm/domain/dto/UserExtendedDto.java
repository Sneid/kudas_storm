package inc.sneid.kudaStorm.domain.dto;

import com.fasterxml.jackson.annotation.JsonView;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.Views;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class UserExtendedDto {

    @JsonView(Views.PUBLIC.class)
    private User user;
    @JsonView(Views.PUBLIC.class)
    private boolean isSubscribed;

}
