package inc.sneid.kudaStorm.controller;

import com.fasterxml.jackson.annotation.JsonView;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.Views;
import inc.sneid.kudaStorm.domain.dto.UserDto;
import inc.sneid.kudaStorm.domain.dto.UserExtendedDto;
import inc.sneid.kudaStorm.domain.dto.UserPageDto;
import inc.sneid.kudaStorm.exceptions.CommonWebException;
import inc.sneid.kudaStorm.service.SubscriptionService;
import inc.sneid.kudaStorm.service.UserService;
import inc.sneid.kudaStorm.utils.RegValid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;
    private final SubscriptionService subscriptionService;

    @Autowired
    public UserController(UserService userService, SubscriptionService subscriptionService) {
        this.userService = userService;
        this.subscriptionService = subscriptionService;
    }

    /**
     * Пользователь может получить данные о себе. Свой объект пользователя
     * @param you
     * @return
     */
    @GetMapping("/me")
    @PreAuthorize("isAuthenticated()")
    @JsonView(Views.PRIVATE.class)
    public User getYourself(@AuthenticationPrincipal User you) {
        return you;
    }

    /**
     * @param userId
     * @return Возвращает пользователя по его id или username(смотря что отправишь)
     */
    @GetMapping("{user}")
    @JsonView(Views.PUBLIC.class)
    public User getUserById(@PathVariable("user") String userId) {
        if (RegValid.fullValid("^\\d+$", userId)){
            return userService.findById(Integer.parseInt(userId));
        }else if (RegValid.fullValid("^([a-zA-Z]+\\w*)$", userId)){
            return userService.findByUsername(userId);
        }
        throw new CommonWebException("Someone error. I don't know!");
    }


    /**
     * @param userId
     * @return Возвращает пользователя по его id или username(смотря что отправишь).
     * А также возвращает логическую переменную, которая отвечает на вопрос "Подписаны ли на пользователя?"
     */
    @GetMapping("/extended/{user}")
    @JsonView(Views.PUBLIC.class)
    @PreAuthorize("isAuthenticated()")
    public UserExtendedDto getUserByIdExtended(@PathVariable("user") String userId,
                                    @AuthenticationPrincipal User me) {
        User user = getUserById(userId);
        boolean isSubscribed = subscriptionService.isSubscriptionExist(user, me);
        return new UserExtendedDto(user, isSubscribed);
    }

    /**
     * Позволяет изменить поля пользователя
     * @param userBody
     * @param you
     * @return
     */
    @PutMapping("/update")
    @PreAuthorize("isAuthenticated()")
    @JsonView(Views.PUBLIC.class)
    public User update(@RequestBody UserDto userBody,
                       @AuthenticationPrincipal User you){
        if (userBody.getId() == null){
            throw new CommonWebException("Id not specified");
        }
        User user = userService.findById(userBody.getId());
        if(user.equals(you)) {
            return userService.update(userBody, user);
        }else {
            throw new CommonWebException("You have no permissions for this action");
        }
    }

    /**
     * @param you Позволяет удалить пользователя
     */
    @DeleteMapping
    @PreAuthorize("isAuthenticated()")
    public void delete(@AuthenticationPrincipal User you){
        userService.remove(you);
    }

    /**
     * Позволяет подписаться на пользователя
     * @param user
     * @param currentUser
     */
    @PostMapping("/subscribe/{user}")
    @PreAuthorize("isAuthenticated()")
    public void subscribe(@PathVariable User user,
                            @AuthenticationPrincipal User currentUser){
        subscriptionService.subscribe(user, currentUser);
    }

    /**
     * Позволяет отписаться от пользователя
     * @param user
     * @param currentUser
     */
    @PostMapping("/unsubscribe/{user}")
    @PreAuthorize("isAuthenticated()")
    public void unsubscribe(@PathVariable User user,
                              @AuthenticationPrincipal User currentUser){
        subscriptionService.unsubscribe(user, currentUser);
    }

    /**
     * @param username
     * @param pageId
     * @param pageSize
     * @return
     */
    @GetMapping("findBy")
    @JsonView(Views.USER_PAGE.class)
    public UserPageDto findBy(@RequestParam(name = "username", defaultValue = "") String username,
                              @RequestParam(name = "p", defaultValue = "0") int pageId,
                              @RequestParam(name = "n", defaultValue = "10") int pageSize){
        Page<User> page = userService.findUserByUsernameStartsWith(username, pageId, pageSize, true);
        return new UserPageDto(page);
    }

    @GetMapping("subscribers")
    @PreAuthorize("isAuthenticated()")
    @JsonView(Views.USER_PAGE.class)
    public UserPageDto subscribers(@AuthenticationPrincipal User user,
                                  @RequestParam(name = "p", defaultValue = "0") int pageId,
                                  @RequestParam(name = "n", defaultValue = "10") int pageSize){
        return subscriptionService.getSubscribersOf(user, pageId, pageSize);
    }

    @GetMapping("subscriptions")
    @PreAuthorize("isAuthenticated()")
    @JsonView(Views.USER_PAGE.class)
    public UserPageDto subscriptions(@AuthenticationPrincipal User user,
                                   @RequestParam(name = "p", defaultValue = "0") int pageId,
                                   @RequestParam(name = "n", defaultValue = "10") int pageSize){
        return subscriptionService.getSubscriptionsOf(user, pageId, pageSize);
    }
}