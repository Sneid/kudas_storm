package inc.sneid.kudaStorm.controller;

import com.fasterxml.jackson.annotation.JsonView;
import inc.sneid.kudaStorm.domain.Role;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.Views;
import inc.sneid.kudaStorm.domain.dto.LoginDto;
import inc.sneid.kudaStorm.domain.dto.RegistrationDto;
import inc.sneid.kudaStorm.exceptions.CommonWebException;
import inc.sneid.kudaStorm.filter.JwtFilter;
import inc.sneid.kudaStorm.jwt.JwtProvider;
import inc.sneid.kudaStorm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@RequestMapping("/api/v1/")
@RestController
@CrossOrigin
public class LoginRegController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Позволяет авторизоваться на сайте. Вы получаете куки токен для авторизации
     * @param loginDto
     * @param response
     * @return
     */
    @PostMapping("/login")
    @JsonView(Views.PUBLIC.class)
    public User loginUser(@RequestBody LoginDto loginDto, HttpServletResponse response){
        User user = userService.findByUsername(loginDto.getUsername());
        if (!passwordEncoder.matches(loginDto.getPassword(), user.getPassword())){
            throw new CommonWebException("Login error exception");
        }
        String token = jwtProvider.generateToken(user.getUsername());
        Cookie cookie = new Cookie(JwtFilter.AUTHORIZATION, token);
        cookie.setPath("/");
        response.addCookie(cookie);
        return user;
    }

    /**
     * Добавляет пользователя в базу данных. Теперь вы можете авторизоваться
     * @param regDto
     * @return
     */
    @PostMapping("/reg")
    @JsonView(Views.PUBLIC.class)
    public User registerUser(@RequestBody RegistrationDto regDto){
        if (userService.isExistsUserWithUsername(regDto.getUsername())){
            throw new CommonWebException("User already exists!");
        }
        User user = new User();
        user.setUsername(regDto.getUsername());
        user.setPassword(passwordEncoder.encode(regDto.getPassword()));
        user.setRoles(Set.of(Role.USER));
        return userService.save(user);
    }
}
