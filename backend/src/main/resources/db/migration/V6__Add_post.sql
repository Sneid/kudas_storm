create table post (
	id int8 NOT null,
	text varchar(2048) not null,
	author_id int8 not null,
	constraint post_pk primary key (id)
);