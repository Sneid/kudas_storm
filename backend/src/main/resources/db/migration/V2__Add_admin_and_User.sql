insert into usr (id, username, password, active, image_name, image_avatar)
    values (1, 'admin', '123', true, 'admin.jpg', 'admin_avatar.jpg'), (2, 'user', '123', true, 'user.jpg', 'user_avatar.jpg');

insert into user_role (user_id, roles)
    values (1, 'USER'), (1, 'ADMIN'), (2, 'USER');
