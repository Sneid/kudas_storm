package inc.sneid.kudaStorm.service;

import inc.sneid.kudaStorm.domain.Subscription;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.dto.UserPageDto;
import inc.sneid.kudaStorm.exceptions.CommonWebException;
import inc.sneid.kudaStorm.repo.SubscriptionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubscriptionService {

    private final SubscriptionRepo subscriptionRepo;

    private final UserService userService;

    @Autowired
    public SubscriptionService(SubscriptionRepo subscriptionRepo, UserService userService) {
        this.subscriptionRepo = subscriptionRepo;
        this.userService = userService;
    }

    public UserPageDto getSubscribersOf(User user, int pageId, int pageSize){
        Pageable pageable = PageRequest.of(pageId,pageSize);
        Page<Subscription> subscribersPage = subscriptionRepo.findByUserId(user.getId(), pageable);
        List<User> subscribers = subscribersPage.get()
                .map(subscription -> userService.findById(subscription.getSubscriberId())).collect(Collectors.toList());
        return new UserPageDto(subscribersPage.getTotalElements(),
                        pageId,
                        subscribers);
    }

    public UserPageDto getSubscriptionsOf(User user, int pageId, int pageSize){
        Pageable pageable = PageRequest.of(pageId, pageSize);
        Page<Subscription> subscriptionPage = subscriptionRepo.findBySubscriberId(user.getId(), pageable);
        List<User> subscriptions = subscriptionPage.get()
                .map(subscription -> userService.findById(subscription.getUserId())).collect(Collectors.toList());
        return new UserPageDto(subscriptionPage.getTotalElements(),
                            pageId,
                            subscriptions);
    }

    public Subscription subscribe(User user, User subscriber){
        if (user.getId().equals(subscriber.getId())){
            throw new CommonWebException("You can't subscribe on you");
        }
        Subscription subscription = new Subscription(user.getId(), subscriber.getId());
        return subscriptionRepo.save(subscription);
    }

    public void unsubscribe(User user, User subscriber){
        Subscription subscription = new Subscription(user.getId(), subscriber.getId());
        subscriptionRepo.delete(subscription);
    }

    public boolean isSubscriptionExist(User user, User subscriber){
        Subscription subscription = subscriptionRepo
                .findByUserIdAndSubscriberId(user.getId(), subscriber.getId()).orElse(null);
        return subscription != null;
    }

    public boolean isSubscriptionExist(Long userId, Long subscriberId){
        Subscription subscription = subscriptionRepo
                .findByUserIdAndSubscriberId(userId, subscriberId).orElse(null);
        return subscription != null;
    }

}
