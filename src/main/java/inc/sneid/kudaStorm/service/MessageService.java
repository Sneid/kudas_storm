package inc.sneid.kudaStorm.service;

import inc.sneid.kudaStorm.domain.Message;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.repo.MessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageRepo messageRepo;

    public void save(Message message){
        messageRepo.save(message);
    }

    public void remove(Message message){
        messageRepo.delete(message);
    }

    public List<Message> findAuthorToRecipient(User author, User recipient){
        return messageRepo.findByAuthorAndRecipient(author, recipient);
    }

    public List<Message> findAuthorAndRecipient(User authorOrRecipient, User recipientOrAuthor){
        var array = messageRepo.findByAuthorAndRecipient(authorOrRecipient, recipientOrAuthor);
        array.addAll(messageRepo.findByAuthorAndRecipient(recipientOrAuthor, authorOrRecipient));
        array.sort((message, t1) -> (int) (message.getId() - t1.getId()));
        return array;
    }

}
