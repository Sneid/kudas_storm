package inc.sneid.kudaStorm.controller;

import com.fasterxml.jackson.annotation.JsonView;
import inc.sneid.kudaStorm.domain.Post;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.Views;
import inc.sneid.kudaStorm.domain.dto.PostPageDto;
import inc.sneid.kudaStorm.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/post")
public class PostController {

    @Autowired
    private PostService postService;

    /**
     * Позволяет получить пост по id
     * @param postId
     * @return
     */
    @GetMapping("/{postId}")
    @JsonView({Views.POST.class})
    public Post getPost(@PathVariable(name = "postId") Long postId){
        return postService.getPost(postId);
    }

    /**
     * Позволяет получить страницу с постами с указанием номера страницы и ее размера
     * @param pageId
     * @param pageSize
     * @return
     */
    @GetMapping("list")
    @JsonView(Views.POST.class)
    public PostPageDto getPostPage(@RequestParam(name = "p", defaultValue = "0") int pageId,
                                   @RequestParam(name = "n", defaultValue = "10") int pageSize){
        return postService.getPostPage(pageId, pageSize);
    }

    /**
     * Позволяет создать новый пост от вашего имени
     * @param post
     * @param user
     * @return
     */
    @PostMapping
    @PreAuthorize("isAuthenticated()")
    @JsonView(Views.POST.class)
    public Post createPost(@RequestBody Post post,
                           @AuthenticationPrincipal User user){
        return postService.savePost(post, user);
    }

    @GetMapping()
    @PreAuthorize("isAuthenticated()")
    @JsonView(Views.LIGHT.class)
    public PostPageDto getUserPosts(@AuthenticationPrincipal User user,
                                   @RequestParam(defaultValue = "0", name = "p") int pageId,
                                   @RequestParam(defaultValue = "10", name = "n") int size){
        return new PostPageDto(postService.findUserPost(user, pageId, size));
    }

    @DeleteMapping("{postId}")
    @PreAuthorize("isAuthenticated()")
    public void delete(@PathVariable(name = "postId") Post post){
        postService.deletePost(post);
    }

}
