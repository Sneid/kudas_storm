package inc.sneid.kudaStorm.controller;

import inc.sneid.kudaStorm.domain.Message;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/message")
public class MessageController {

    @Autowired
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("send/{user}")
    public Message send(@PathVariable User user,
                       @AuthenticationPrincipal User currentUser,
                       Message message){
        message.setAuthor(currentUser);
        message.setRecipient(user);
        messageService.save(message);
        return message;
    }

}
