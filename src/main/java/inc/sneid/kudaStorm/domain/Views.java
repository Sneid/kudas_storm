package inc.sneid.kudaStorm.domain;

public class Views {
    public static class PUBLIC{}
    public static class PRIVATE extends PUBLIC{}
    public static class POST extends PUBLIC{}
    public static class LIGHT{}
    public static class USER_PAGE extends PUBLIC{}
}
