package inc.sneid.kudaStorm.domain.key;

import java.io.Serializable;
import java.util.Objects;

public class SubscriptionKey implements Serializable {
    private Long userId;
    private Long subscriberId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionKey that = (SubscriptionKey) o;
        return Objects.equals(userId, that.userId) && Objects.equals(subscriberId, that.subscriberId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, subscriberId);
    }
}
