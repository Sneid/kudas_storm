package inc.sneid.kudaStorm.domain.dto;

import com.fasterxml.jackson.annotation.JsonView;
import inc.sneid.kudaStorm.domain.User;
import inc.sneid.kudaStorm.domain.Views;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
public class UserPageDto {

    @JsonView(Views.USER_PAGE.class)
    private long totalElements;
    @JsonView(Views.USER_PAGE.class)
    private int pageId;
    @JsonView(Views.USER_PAGE.class)
    private List<User> users;

    public UserPageDto(Page<User> page ) {
        this.totalElements = page.getTotalElements();
        this.users = page.getContent();
        this.pageId = page.getNumber();
    }

    public UserPageDto(long totalElements, int pageId, List<User> users) {
        this.totalElements = totalElements;
        this.pageId = pageId;
        this.users = users;
    }
}