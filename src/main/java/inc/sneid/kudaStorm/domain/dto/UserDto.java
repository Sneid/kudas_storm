package inc.sneid.kudaStorm.domain.dto;

import inc.sneid.kudaStorm.domain.User;
import lombok.*;

@Getter
@AllArgsConstructor
@ToString
@Builder(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String username;
    private Boolean active;

    public UserDto(User user) {
        id = user.getId();
        username = user.getUsername();
        active = user.isActive();
    }
}
