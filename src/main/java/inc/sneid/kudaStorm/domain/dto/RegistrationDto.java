package inc.sneid.kudaStorm.domain.dto;

import lombok.Data;

@Data
public class RegistrationDto {
    private String username;
    private String password;
}
