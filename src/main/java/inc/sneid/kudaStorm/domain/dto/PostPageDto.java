package inc.sneid.kudaStorm.domain.dto;

import com.fasterxml.jackson.annotation.JsonView;
import inc.sneid.kudaStorm.domain.Post;
import inc.sneid.kudaStorm.domain.Views;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
public class PostPageDto {

    @JsonView({Views.POST.class, Views.LIGHT.class})
    private long totalElements;
    @JsonView({Views.POST.class, Views.LIGHT.class})
    private List<Post> posts;
    @JsonView({Views.POST.class, Views.LIGHT.class})
    private int pageId;

    public PostPageDto(Page<Post> page) {
        this.totalElements = page.getTotalElements();
        this.posts = page.getContent();
        this.pageId = page.getNumber();
    }
}
