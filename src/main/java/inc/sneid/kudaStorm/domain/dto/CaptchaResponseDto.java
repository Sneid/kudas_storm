package inc.sneid.kudaStorm.domain.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class CaptchaResponseDto {
    private boolean success;
    private float score;
    @JsonAlias("error-codes")
    private Set<String> errorCodes;
}
