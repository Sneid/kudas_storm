package inc.sneid.kudaStorm.domain;

import inc.sneid.kudaStorm.domain.key.SubscriptionKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "user_subscriptions")
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(SubscriptionKey.class)
public class Subscription {

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Id
    @Column(name = "subscriber_id")
    private Long subscriberId;
}
