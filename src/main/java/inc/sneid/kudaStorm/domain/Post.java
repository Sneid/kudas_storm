package inc.sneid.kudaStorm.domain;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "post")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    @Column(nullable = false)
    @Id
    @JsonView({Views.POST.class, Views.LIGHT.class})
    private Long id;

    @JsonView({Views.POST.class, Views.LIGHT.class})
    private String text;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    @JsonView(Views.POST.class)
    private User author;
}
