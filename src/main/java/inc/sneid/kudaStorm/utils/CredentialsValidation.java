package inc.sneid.kudaStorm.utils;

public class CredentialsValidation {

    /**
     * @param username Этот метод проверяет на правильность имя пользователя. В нем должны быть только цифры
     *                и английские буквы
     * @return
     */
    public static boolean isValidUsername(String username){
        if (username == null || username.trim().isEmpty()) return false;
        return RegValid.fullValid("^[a-zA-Z0-9]{3,}$", username);
    }

    public static boolean isValidPassword(String password){
        return true;
    }

}
