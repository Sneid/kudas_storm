package inc.sneid.kudaStorm.utils;

import inc.sneid.kudaStorm.domain.Post;

public class PostValidation {

    public static boolean isValid(Post post){
        if (RegValid.fullValid("^.{1,200}$", post.getText().strip())){
            return true;
        }
        return false;
    }

}
