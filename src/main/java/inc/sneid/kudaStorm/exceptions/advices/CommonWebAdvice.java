package inc.sneid.kudaStorm.exceptions.advices;

import inc.sneid.kudaStorm.exceptions.CommonWebException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CommonWebAdvice {

    @ResponseBody
    @ExceptionHandler(CommonWebException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    String commonWebHandler(CommonWebException e){
        return e.getMessage();
    }

}
