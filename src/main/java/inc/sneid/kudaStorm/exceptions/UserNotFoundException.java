package inc.sneid.kudaStorm.exceptions;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(long id){
        super("Couldn't find user with id: " + id);
    }

    public UserNotFoundException(String username){
        super("Couldn't find user with username: " + username);
    }
}
