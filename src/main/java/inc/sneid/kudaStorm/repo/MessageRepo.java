package inc.sneid.kudaStorm.repo;

import inc.sneid.kudaStorm.domain.Message;
import inc.sneid.kudaStorm.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepo extends JpaRepository<Message, Long> {

    List<Message> findByAuthorAndRecipient(User author, User recipient);

}
