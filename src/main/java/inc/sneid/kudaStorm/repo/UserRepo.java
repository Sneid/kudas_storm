package inc.sneid.kudaStorm.repo;

import inc.sneid.kudaStorm.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Page<User> findByUsernameStartingWithIgnoreCase(String string, Pageable pageable);

    Page<User> findByUsernameStartingWith(String string, Pageable pageable);

}
