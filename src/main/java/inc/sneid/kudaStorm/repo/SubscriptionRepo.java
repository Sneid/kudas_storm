package inc.sneid.kudaStorm.repo;

import inc.sneid.kudaStorm.domain.Subscription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SubscriptionRepo extends JpaRepository<Subscription, Long> {

    Page<Subscription> findByUserId(Long userId, Pageable pageable);

    Page<Subscription> findBySubscriberId(Long subscriberId, Pageable pageable);

    Optional<Subscription> findByUserIdAndSubscriberId(Long userId, Long subscriberId);

}
