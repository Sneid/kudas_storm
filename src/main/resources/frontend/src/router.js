import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/buefy/Home.vue";
import About from "@/components/buefy/About.vue";
import Profile from '@/components/buefy/Profile.vue'
import UserFind from '@/components/buefy/UserFind.vue'
import PostErrorSimple from "@/components/buefy/PostErrorSimple.vue";
import ProfileStranger from "@/components/buefy/ProfileStranger.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Home,
    },
    {
      path: "/about",
      component: About,
    },
    {
      path: "/profile",
      component: Profile,
    },
    {
      path: "/users",
      component: UserFind
    },
    {
      path: "/profile/:username",
      component: ProfileStranger,
    },
    {
      path: "/*",
      component: PostErrorSimple,
    }
  ],
});
