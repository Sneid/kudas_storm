// vue.config.js
module.exports = {
  // options...
  devServer: {
    port: 3000,
    proxy: {
      // "/message-ws": {
      //   target: "ws://localhost:8080/message-ws",
      //   secure: false,
      //   ws: true,
      // },
      "/": { target: "http://localhost:8080/", autoRewrite: true },
    },
  },
  outputDir: "../static",
};
