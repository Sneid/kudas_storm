create table user_subscriptions (
    user_id int8 not null references usr,
    subscriber_id int8 not null references usr,
    primary key(user_id, subscriber_id)
)