create sequence hibernate_sequence start 1 increment 1;

create table user_role (
    user_id int8 not null,
    roles varchar(255)
);

create table usr (
    id int8 not null,
    active boolean not null,
    image_name varchar(255),
    image_avatar varchar(255),
    password varchar(255),
    username varchar(255),
    primary key (id)
);

alter table if exists user_role
    add constraint FKfpm8swft53ulq2hl11yplpr5
    foreign key (user_id) references usr;

alter table if exists user_subscriptions
    add constraint FKm69uaasbua17sgdnhsq10yxd5
    foreign key (subscriber_id) references usr;

alter table if exists message
    add constraint message_user_fk
    foreign key (user_id) references usr;


