var profileStranger = Vue.component("ProfileStranger", {
    template: `
    <div>
        <b-notification v-if="isLoading" :closable="false">
        <b-loading
            :is-full-page="false"
            v-model="isLoading"
            :can-cancel="false"
        ></b-loading>
        </b-notification>
        <div v-else class="fbox-profile-stranger profile-page-profile-stranger">
        <div id="cont1-profile-stranger" class="fbox-profile-stranger column-profile-stranger white-profile-stranger">
            <div class="profile-image-profile-stranger">
            <img src="https://picsum.photos/350/350" alt="A random image" />
            </div>
            <div class="buttons-profile-stranger center-bar-profile-stranger">
            <b-button @click="subscribe" v-if="!isSubscribed" type="is-info"
                >Подписаться</b-button
            >
            <b-button @click="unsubscribe" v-else type="is-info is-light"
                >Отписаться</b-button
            >
            </div>
            <div>
            <p class="text-profile-stranger">
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deleniti,
                quidem dolores voluptatem ipsa necessitatibus eos placeat qui
                ducimus architecto ad hic? Qui explicabo labore impedit eos amet
                distinctio nam nulla.
            </p>
            </div>
        </div>

        <div id="cont2-profile-stranger" class="white-profile-stranger">
            <p id="title-profile-stranger">Профиль</p>
            <b-tabs position="is-centered" class="block tabs-sizes">
            <b-tab-item label="Главная">
                <p>
                Имя пользователя <b>{{ someUser.username }}</b>
                </p>
            </b-tab-item>
            </b-tabs>
        </div>
        </div>
    </div>
    `,
    props: ["isAuthorized", "user"],
    data() {
        return {
            someUser: {},
            isSubscribed: false,
            isLoading: true,
        };
    },
    methods: {
        subscribe() {
            console.log("subscribe");
            this.$http.post("/api/v1/user/subscribe/" + this.someUser.id).then(() => {
                this.isSubscribed = true;
            });
        },
        unsubscribe() {
            console.log("unsubscribe");
            this.$http.post("/api/v1/user/unsubscribe/" + this.someUser.id).then(() => {
                this.isSubscribed = false;
            });
        },
    },
    mounted() {
        if (
            sessionStorage.getItem("username") == this.$route.params.username ||
            sessionStorage.getItem("id") == this.$route.params.username
        ) {
            this.$router.push({ path: "/profile" });
            return;
        }
        var progress = () => {
            this.isLoading = false;
        };
        this.$http
            .get("/api/v1/user/extended/" + this.$route.params.username, {
                progress: progress,
            })
            .then(
                (response) => {
                    this.someUser = response.body.user;
                    this.isSubscribed = response.body.isSubscribed;
                },
                () => {
                    this.$router.push({ path: "/" });
                    return;
                }
            );
    },
})