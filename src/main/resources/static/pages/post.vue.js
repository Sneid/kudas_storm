Vue.component("Post", {
    template: `
    <div class="mypost">
    <section>
      <article class="media">
        <figure class="media-left">
          <router-link :to="'/profile/' + post.author.username">
            <p class="image is-64x64">
              <img
                src="https://d1nhio0ox7pgb.cloudfront.net/_img/o_collection_png/green_dark_grey/512x512/plain/user.png"
              />
            </p>
          </router-link>
        </figure>
        <div class="media-content">
          <div class="content">
            <p>
              {{ post.text }}
            </p>
          </div>
          <nav class="level">
            <div class="level-left">
              <a class="level-item"> </a>
            </div>
            <div class="level-right">
              <a v-if="post.author.username" class="level-item">
                <router-link :to="'/profile/' + post.author.username">
                  {{ post.author.username }}
                </router-link>
              </a>
            </div>
          </nav>
        </div>
      </article>
    </section>
  </div>
    `,
    props: ["post"],
    created() {
        if (this.post.author === undefined) {
            this.post.author = { username: "" };
        }
    },
})