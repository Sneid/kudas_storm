var navBar = Vue.component("NavBar", {
    template: `
    <div>
    <b-navbar>
      <template #brand>
        <b-navbar-item tag="router-link" :to="{ path: '/' }">
          <img src="https://i.imgur.com/F1WrvyK.png" alt="Logo of KudaStorm" />
        </b-navbar-item>
      </template>
      <template #start>
        <b-navbar-item>
          <router-link to="/">Главная</router-link>
        </b-navbar-item>
        <b-navbar-item>
          <router-link to="/users">Пользователи</router-link>
        </b-navbar-item>
        <b-navbar-item>
          <router-link to="/chat">Общий чатик</router-link>
        </b-navbar-item>
        <b-navbar-item>
          <router-link to="/about">О нас</router-link>
        </b-navbar-item>
      </template>

      <template #end>
        <!-- Если ты не авторизован, то делай это -->
        <b-navbar-item v-if="!isAuthorized" tag="div">
          <section>
            <div class="buttons">
              <a class="button is-primary">
                <strong>Sign up</strong>
              </a>
              <section>
                <button
                  class="button is-light"
                  @click="isComponentModalActive = true"
                >
                  Log in
                </button>
              </section>
            </div>
          </section>
        </b-navbar-item>
        <!-- Иначе делай это -->
        <b-navbar-item v-else tag="div">
          <section>
            <div class="buttons">
              <p>{{ user.username }}</p>
              <router-link to="/profile">
                <img
                  style="padding-left: 10px; padding-right: 10px"
                  src="https://d1nhio0ox7pgb.cloudfront.net/_img/o_collection_png/green_dark_grey/512x512/plain/user.png"
                  alt="Image not found"
                />
              </router-link>
              <button @click="logout()" class="button is-primary">
                <strong>Log out</strong>
              </button>
            </div>
          </section>
        </b-navbar-item>
      </template>
    </b-navbar>
    <b-modal :active.sync="isComponentModalActive" has-modal-card>
      <form>
        <div class="modal-card" style="width: auto">
          <header class="modal-card-head">
            <p class="modal-card-title">Login</p>
          </header>
          <section class="modal-card-body">
            <b-field label="Username">
              <b-input
                type="text"
                :value="username"
                v-model="username"
                validation-message="Это поле не должно быть пустым"
                placeholder="Your username"
                required
              >
              </b-input>
            </b-field>

            <b-field
              label="Password"
              :type="{ 'is-danger': passwordError }"
              :message="{ 'Пароль или логин неправильный': passwordError }"
            >
              <b-input
                :value="password"
                v-model="password"
                type="password"
                password-reveal
                validation-message="Нужно написать свой настоящий пароль"
                placeholder="Your password"
                required
              >
              </b-input>
            </b-field>
          </section>
          <footer class="modal-card-foot">
            <button
              class="button"
              type="button"
              @click="isComponentModalActive = false"
            >
              Close
            </button>
            <button type="button" @click="login()" class="button is-primary">
              Login
            </button>
          </footer>
        </div>
      </form>
    </b-modal>
  </div>
    `,
    props: ["isAuthorized", "user"],
    data() {
        return {
            isComponentModalActive: false,
            username: "",
            password: "",
            passwordError: false,
        };
    },
    methods: {
        login() {
            if (!(this.username.trim() === "" || this.password.trim() === "")) {
                this.$http
                    .post("/api/v1/login", {
                        username: this.username,
                        password: this.password,
                    })
                    .then(
                        () => {
                            this.loginOK();
                        },
                        () => {
                            this.loginFAIL();
                        }
                    );
            }
        },
        logout() {
            this.eraseCookie("Authorization");
            sessionStorage.clear();
            this.$router.replace({ path: "/" });
            document.location.reload();
        },
        loginOK() {
            document.location.reload();
        },
        loginFAIL() {
            this.password = "";
            this.passwordError = true;
        },
        setCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "") + expires + "; path=/";
        },
        getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(";");
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == " ") c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        },
        eraseCookie(name) {
            document.cookie =
                name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
        },
    },
})