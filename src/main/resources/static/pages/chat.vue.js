var chat = Vue.component("Chat", {
    template: `
    <div>
        <div v-for="message in messages" :key="message.id">
            <p>{{message.text}}</p>
        </div>
        <div>
            <input v-model="text" type="text">
            <button @click="sendName(text)">OK</button>
        </div>
    </div>
    `,
    data() {
        return {
            text: "",
            messages: [
                { id: 0, text: "qwerr" },
                { id: 1, text: "qdfgfdf" },
                { id: 2, text: "gfgjhk" },
                { id: 3, text: "xcvcbnm" },
            ],
            stompClient: null
        }
    },
    methods: {
        connect() {
            var socket = new SockJS('/gs-guide-websocket');
            this.stompClient = Stomp.over(socket);
            this.stompClient.connect({}, this.frameHandler);
        },
        sendName(name) {
            this.stompClient.send("/app/hello", {}, JSON.stringify({ 'name': name }));
        },
        frameHandler(frame) {
            console.log('Connected status: ' + frame);
            this.stompClient.subscribe('/topic/greetings', this.addMessage);
        },
        addMessage(greeting){
            this.messages.push({ id: this.messages[this.messages.length - 1].id + 1, text: JSON.parse(greeting.body).content })
        }
    },
    mounted() {
        this.connect()
    }
})