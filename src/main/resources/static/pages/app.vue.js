var app = Vue.component("App", {
    template: `
        <div id="app">
            <NavBar :isAuthorized="isAuthorized" :user="user" />
            <router-view :isAuthorized="isAuthorized" :user="user" />
        </div>
    `,
    data() {
        return {
            isAuthorized: false,
            user: {},
        };
    },
    beforeCreate() {
        if (document.cookie) {
            this.$http.get("/api/v1/user/me").then(
                (response) => {
                    this.isAuthorized = true;
                    this.user = response.body;
                    sessionStorage.setItem("username", response.body.username);
                    sessionStorage.setItem("id", response.body.id)
                },
                () => {
                    this.isAuthorized = false;
                    this.user = {};
                    sessionStorage.clear();
                }
            );
        }
    },
})