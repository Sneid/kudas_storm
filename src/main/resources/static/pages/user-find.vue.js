var trigger = true;

var userFind = Vue.component("UserFind", {
    template: `
    <div>
        <b-field class="find-field" label="Найти пользователя">
        <b-input @input="updateUserInList(username)" v-model="username"></b-input>
        </b-field>
        <div class="user-list">
        <article v-for="user in users" :key="user.id" class="media white">
            <figure class="media-left">
            <p class="image is-64x64">
                <img src="https://bulma.io/images/placeholders/128x128.png" />
            </p>
            </figure>
            <div class="media-content">
            <div class="content">
                <p>
                <router-link :to="'/profile/' + user.username">
                    <strong>{{ user.username }}</strong>
                </router-link>
                <br />
                Some info
                </p>
            </div>
            </div>
        </article>
        </div>
    </div>
    `,
    data() {
        return {
            users: [],
            pageId: 0,
            pageSize: 7,
            userError: false,
            username: "",
        };
    },
    methods: {
        getUsersByUsername(username, pageId, size) {
            this.$http
                .get("/api/v1/user/findBy", {
                    params: { p: pageId, n: size, username: username },
                })
                .then(
                    (response) => {
                        if (response.body.users.length != 0) {
                            this.users.splice(this.users.length, 0, ...response.body.users);
                            this.pageId += 1;
                            trigger = true;
                        }
                    },
                    () => {
                        this.userError = true;
                    }
                );
        },
        handleScroll() {
            if (this.userError) return;
            var isScrolled =
                window.scrollY + window.innerHeight >= document.body.clientHeight - 10;
            if (isScrolled && trigger) {
                console.log("Yeeee");
                trigger = false;
                this.getUsersByUsername("", this.pageId, this.pageSize);
            }
        },
        updateUserInList(newUsername) {
            this.pageId = 0;
            this.$http
                .get("user/findBy", {
                    params: { p: this.pageId, n: this.pageSize, username: newUsername },
                })
                .then(
                    (response) => {
                        if (response.body.users.length != 0) {
                            this.users = [];
                            this.users.splice(0, 0, ...response.body.users);
                            this.pageId += 1;
                            trigger = true;
                        }
                    },
                    () => {
                        this.userError = true;
                    }
                );
        },
    },
    created() {
        window.addEventListener("scroll", this.handleScroll);
    },
    destroyed() {
        window.removeEventListener("scroll", this.handleScroll);
    },
    mounted() {
        this.getUsersByUsername("", this.pageId, this.pageSize);
    },
})