var postErrorSimple = Vue.component("PostErrorSimple", {
    template: `
    <div>
        <!-- purple x moss 2020 -->
        <div class="mainbox-post-error-simple div-post-error-simple">
            <div class="err-post-error-simple">4</div>
                <i class="far fa-question-circle fa-spin"></i>
                <div class="err2-post-error-simple">4</div>
                <div class="msg-post-error-simple">
                    Maybe this page moved? Got deleted? Is hiding out in quarantine? Never
                    existed in the first place?
                <p>Let's go <a class="a-post-error-simple" href="/">home</a> and try from there.</p>
            </div>
        </div>
    </div>
    `
})