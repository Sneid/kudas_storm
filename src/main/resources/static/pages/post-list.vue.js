var trigger = true;

Vue.component("PostList", {
    template: `
    <div>
        <div id="list" class="post-list" v-if="!postError">
            <post-form v-if="isAuthorized" :isAuthorized="isAuthorized" :user="user" :posts="posts" class="mypost" />
            <Post v-for="post in posts" :key="post.id" :post="post" />
        </div>
    <post-error-simple v-else />
     <!-- <post-error-great/> -->
  </div>
    `,
    props: ["posts", "isAuthorized", "user"],
    data() {
        return {
            postError: false,
            pageId: 0,
            pageSize: 7,
        };
    },
    created() {
        window.addEventListener("scroll", this.handleScroll);
    },
    destroyed() {
        window.removeEventListener("scroll", this.handleScroll);
    },
    methods: {
        getPosts(pageId, size) {
            this.$http
                .get("/api/v1/post/list", {
                    params: { p: pageId, n: size },
                })
                .then(
                    (response) => {
                        if (response.body.posts.length != 0) {
                            this.posts.splice(this.posts.length, 0, ...response.body.posts);
                            this.pageId += 1;
                            trigger = true;
                        }
                    },
                    () => {
                        this.postError = true;
                    }
                );
        },
        handleScroll() {
            if (this.postError) return;
            var isScrolled =
                window.scrollY + window.innerHeight >= document.body.clientHeight - 10;
            if (isScrolled && trigger) {
                console.log("Yeeee");
                trigger = false;
                this.getPosts(this.pageId, this.pageSize);
            }
        },
    },
    mounted() {
        this.getPosts(0, 7);
    },
})