var trigger = true;

var profile = Vue.component("Profile", {
    template: `
    <div class="box-profile inline-profile profile-page-profile">
        <div id="cont1-profile" class="box-profile column-profile white-profile">
        <div class="profile-image-profile">
            <img src="https://picsum.photos/350/350" alt="A random image" />
        </div>
        <div>
            <p class="text-profile">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deleniti,
            quidem dolores voluptatem ipsa necessitatibus eos placeat qui ducimus
            architecto ad hic? Qui explicabo labore impedit eos amet distinctio
            nam nulla.
            </p>
        </div>
        </div>
        <div id="cont2-profile" class="white-profile">
        <p id="title-profile">Профиль</p>
        <b-tabs position="is-centered" class="block">
            <b-tab-item label="Главная">
            <p>
                Имя пользователя <b>{{ user.username }}</b>
            </p>
            </b-tab-item>
            <b-tab-item label="Посты">
            <div v-if="userPosts.length == 0">У вас нет постов</div>
            <div v-else-if="!postError">
                <Post v-for="post in userPosts" :key="post.id" :post="post" />
            </div>
            <div v-else>Упс у ваш ошибка с постами</div>
            </b-tab-item>
        </b-tabs>
        </div>
    </div>
    `,
    props: ["isAuthorized", "user"],
    data() {
        return {
            userPosts: [],
            postError: false,
            pageId: 0,
            pageSize: 7,
        };
    },
    methods: {
        getUserPosts(pageId, size) {
            this.$http
                .get("/api/v1/post", {
                    params: { p: pageId, n: size },
                })
                .then(
                    (response) => {
                        if (response.body.posts.length != 0) {
                            this.userPosts.splice(
                                this.userPosts.length,
                                0,
                                ...response.body.posts
                            );
                            this.pageId += 1;
                            trigger = true;
                        }
                    },
                    () => {
                        this.postError = true;
                    }
                );
        },
        handleScroll() {
            if (this.postError) return;
            var isScrolled =
                window.scrollY + window.innerHeight >= document.body.clientHeight - 10;
            if (isScrolled && trigger) {
                trigger = false;
                this.getUserPosts(this.pageId, this.pageSize);
            }
        },
    },
    created() {
        window.addEventListener("scroll", this.handleScroll);
    },
    destroyed() {
        window.removeEventListener("scroll", this.handleScroll);
    },
    mounted() {
        if (!sessionStorage.getItem("id")) {
            this.$router.push({ path: '/' })
            return;
        }
        this.getUserPosts(0, this.pageSize);
    },
})