var home = Vue.component("Home", {
    template: `
    <div>
        <PostList :posts="posts" :isAuthorized="isAuthorized" :user="user" />
    </div>
    `,
    props: ["isAuthorized", "user"],
    data() {
        return {
            posts: [],
        };
    },
})