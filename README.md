# KudaStorm

![Image of Yaktocat](images/image1.png)
![Image of Yaktocat](images/image2.png)
![Image of Yaktocat](images/image3.png)
![Image of Yaktocat](images/image4.png)
![Image of Yaktocat](images/image5.png)

# Тут будет краткое руководство и описание того как запустить проект и установить и настроить все зависимости

### Для начала вам понадобится:
- *Postgresql 12* или новее(гайд который я использовал для установки и настройки https://losst.ru/ustanovka-postgresql-ubuntu-16-04)
- Нужно скачать и запустить проект *https://github.com/Denisius664/media-server*
- Установить на ваш компьютер *Apache Maven 3.6.3*
- Установить *java-14* или новее

### Если хочешь поподробнее узнать что я использовал при разработке и увидеть более подробные инструкции, то читай DevDiary.md

# REST Api
### User api
- Public user json object(**PUJO**) имеет следующие поля: id, username, active, roles 
- Private user json object(**PRUJO**) имеет следующие поля: id, username, active, roles
- **GET 'user/me'** - дает ваш объект PRUJO
- **GET 'user/{userId}'** - при изменении {userId} вы получите PUJO пользователя с указанным id
- **PUT 'user/update'** - потом напишу
- **DELETE 'user'** - удаляет пользователя если вы удаляете себя или если вы администратор
- **GET 'user/subscribe/{userId}'** - при изменении {userId} вы подписываете себе на пользователя с id={userId}
- **GET 'user/unsubscribe/{userId}'** - при изменении {userId} вы отписываете себе от пользователя с id={userId}
- **POST 'post'** - создает новый пост от вашего имени
### Post api
- **GET 'post/{postId}'** - возвращает пост по id